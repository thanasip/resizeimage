﻿using SharpShell.Attributes;
using SharpShell.SharpContextMenu;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ImageResizeCMEntry {
    public static class StringExt {
        public static bool CheckIfImage(this string path) {
            if (path.Length < 4 || string.IsNullOrEmpty(path)) return false;
            var ext = path.Substring(path.Length - 4, 4).ToLower();
            if (ext.Equals(".png") || ext.Equals(".jpg") || ext.Equals(".bmp"))
                return true;
            return false;
        }

        public static string Get3CharExt(this string path) {
            if (path.Length < 4 || string.IsNullOrEmpty(path)) return "";
            return path.Substring(path.Length - 4, 4);
        }
    }

    [ComVisible(true)]
    [COMServerAssociation(AssociationType.AllFiles)]
    public class ImageResizeCMEntry : SharpContextMenu {
        protected override bool CanShowMenu() {
            return true;
        }

        protected override ContextMenuStrip CreateMenu() {
            var menu = new ContextMenuStrip();

            var itemResizeImage = new ToolStripMenuItem {
                Text = "Resize Image",
                Image = Properties.Resources.ResizeIcon16,
            };

            var subItems = new List<ToolStripMenuItem>();

            for (int i = 10; i <= 200; i += 10) {
                var item = new ToolStripMenuItem {
                    Text = $"Resize to {i}%",
                    Image = Properties.Resources.ResizeIcon16
                };

                var per = i;

                item.Click += (sender, e) => ResizeImage(per);

                itemResizeImage.DropDownItems.Add(item);
            }

            menu.Items.Add(itemResizeImage);

            return menu;
        }

        private void ResizeImage(int percent) {
            foreach (var path in SelectedItemPaths) {
                if (path.CheckIfImage()) {
                    var factor = percent / 100.0;
                    using (Image img = Image.FromFile(path)) {
                        int width = (int)Math.Floor(img.Width * factor);
                        int height = (int) Math.Floor(img.Height * factor);
                        using (Image newImg = new Bitmap(img, width, height)) {
                            img.Dispose();
                            newImg.Save(path);
                        }
                    }
                } else {
                    MessageBox.Show($"{path} is NOT an image");
                }
            }
        }
    }
}
